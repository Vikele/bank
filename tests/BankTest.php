<?php
use PHPUnit\Framework\TestCase;

class BankTest extends \PHPUnit\Framework\TestCase
{
    private $bank;

    protected function setUp(): void
{
    $this->bank = new App\Bank();
}

    protected function tearDown(): void
{
    isset($this->bank);
}

    public function testMax(){
    $result = $this->bank->maxPercent();
    $this->assertEquals('D1', $result);
}

    /**
     * *@dataProvider newDataProvider
     * @param $a
     * @param $expected
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     */
    public function testBankManyTimes($a, $expected){
    $result = $this->bank->counTermDeposit($a);
    $this->assertEquals($expected, $result);
}

    public function newDataProvider(){
    return array(
        array(12, 2),
        array(10, 2),
        array(14, 2)
    );
}

}