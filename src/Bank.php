<?php


namespace App;

include 'Deposit.php';
class Bank
{
    private $deposit;

    public function __construct()
    {
        $this->deposit = array(
            new Deposit('D1', 25, 10),
            new Deposit('D1', 55, 10),
            new Deposit('D1', 25.5, 12),
            new Deposit('D1', 24, 12),
            new Deposit('D1', 23, 14),
        ) ;
    }


    public function counTermDeposit($term){
        $count = 0;

        foreach ($this->deposit as $deposit){
            if($deposit->getTerm() == $term){
                $count++;
            }
        }
        return $count;
    }

    public function maxPercent(){
        $b=100;
        foreach($this->deposit as $deposit){
            $a = $deposit->getTerm();
            if($a<=$b){
                $b=$a;
            }
        }
        foreach ($this->deposit as $deposit){
            if($deposit->getTerm()==$b)
                return $deposit->getDepositName();
        }
    }
}