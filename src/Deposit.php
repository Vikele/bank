<?php


namespace App;


class Deposit
{
  private $deposit_name;
  private $percent;
  private $term;

    /**
     * Deposit constructor.
     * @param $deposit_name
     * @param $percent
     * @param $term
     */
    public function __construct($deposit_name, $percent, $term)
    {
        $this->deposit_name = $deposit_name;
        $this->percent = $percent;
        $this->term = $term;
    }

    /**
     * @return mixed
     */
    public function getDepositName()
    {
        return $this->deposit_name;
    }

    /**
     * @param mixed $deposit_name
     */
    public function setDepositName($deposit_name)
    {
        $this->deposit_name = $deposit_name;
    }

    /**
     * @return mixed
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param mixed $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return mixed
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param mixed $term
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }


}